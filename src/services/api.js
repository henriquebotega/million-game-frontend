import axios from "axios";

const api = axios.create({
	baseURL: "https://million-game-backend.herokuapp.com/api"
});

export default api;
