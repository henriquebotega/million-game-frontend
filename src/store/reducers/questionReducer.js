const INITIAL_VALUE = {
	loading: false,
	data: {},
	error: false,
	msgError: ""
};

export const questionReducer = (state = INITIAL_VALUE, action) => {
	switch (action.type) {
		case "GET_QUESTION_REQUEST":
			return {
				...state,
				error: false,
				msgError: "",
				loading: true
			};
		case "GET_QUESTION_SUCCESS":
			return {
				...state,
				data: action.payload,
				loading: false
			};
		case "GET_QUESTION_FAILURE":
			return {
				...state,
				data: {},
				error: true,
				msgError: action.payload,
				loading: false
			};

		// RESET GAME
		case "RESET_GAME":
		case "RESET_QUESTION":
			return INITIAL_VALUE;

		default:
			return state;
	}
};
