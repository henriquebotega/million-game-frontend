const INITIAL_VALUE = {
	loading: false,
	data: {},
	error: false,
	msgError: ""
};

export const userReducer = (state = INITIAL_VALUE, action) => {
	switch (action.type) {
		case "CREATE_USER_REQUEST":
			return {
				...state,
				error: false,
				msgError: "",
				loading: true
			};
		case "CREATE_USER_SUCCESS":
			return {
				...state,
				data: action.payload,
				loading: false
			};
		case "CREATE_USER_FAILURE":
			return {
				...state,
				data: {},
				error: true,
				msgError: action.payload,
				loading: false
			};

		// RESET GAME
		case "RESET_GAME":
			return INITIAL_VALUE;

		default:
			return state;
	}
};
