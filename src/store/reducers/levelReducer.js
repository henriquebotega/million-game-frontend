const INITIAL_VALUE = {
	loading: false,
	data: [],
	error: false,
	msgError: ""
};

export const levelReducer = (state = INITIAL_VALUE, action) => {
	switch (action.type) {
		case "GET_LEVEL_REQUEST":
			return {
				...state,
				error: false,
				msgError: "",
				loading: true
			};
		case "GET_LEVEL_SUCCESS":
			return {
				...state,
				data: action.payload,
				loading: false
			};
		case "GET_LEVEL_FAILURE":
			return {
				...state,
				data: [],
				error: true,
				msgError: action.payload,
				loading: false
			};

		// RESET GAME
		case "RESET_GAME":
			return INITIAL_VALUE;

		default:
			return state;
	}
};
