import { combineReducers } from "redux";
import { answerReducer } from "./answerReducer";
import { gameReducer } from "./gameReducer";
import { levelReducer } from "./levelReducer";
import { questionReducer } from "./questionReducer";
import { userReducer } from "./userReducer";

export default combineReducers({ answerReducer, levelReducer, questionReducer, userReducer, gameReducer });
