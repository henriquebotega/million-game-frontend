const INITIAL_VALUE = {
	loading: false,
	data: [],
	error: false,
	msgError: ""
};

export const answerReducer = (state = INITIAL_VALUE, action) => {
	switch (action.type) {
		case "GET_ANSWER_BY_QUESTION_REQUEST":
			return {
				...state,
				error: false,
				msgError: "",
				loading: true
			};
		case "GET_ANSWER_BY_QUESTION_SUCCESS":
			return {
				...state,
				data: action.payload,
				loading: false
			};
		case "GET_ANSWER_BY_QUESTION_FAILURE":
			return {
				...state,
				data: [],
				error: true,
				msgError: action.payload,
				loading: false
			};

		// RESET GAME
		case "RESET_GAME":
		case "RESET_ANSWER":
			return INITIAL_VALUE;

		default:
			return state;
	}
};
