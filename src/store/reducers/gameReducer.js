const INITIAL_VALUE = {
	loading: false,
	data: {},
	error: false,
	msgError: ""
};

export const gameReducer = (state = INITIAL_VALUE, action) => {
	switch (action.type) {
		// SKIP QUESTION
		case "SKIP_QUESTION_REQUEST":
			return {
				...state,
				error: false,
				msgError: "",
				loading: true
			};
		case "SKIP_QUESTION_SUCCESS":
			return {
				...state,
				data: action.payload,
				loading: false
			};
		case "SKIP_QUESTION_FAILURE":
			return {
				...state,
				error: true,
				msgError: action.payload,
				loading: false
			};

		// FIFTY QUESTION
		case "FIFTY_QUESTION_REQUEST":
			return {
				...state,
				error: false,
				msgError: ""
			};
		case "FIFTY_QUESTION_SUCCESS":
			return {
				...state,
				data: action.payload
			};
		case "FIFTY_QUESTION_FAILURE":
			return {
				...state,
				error: true,
				msgError: action.payload
			};

		// GAME
		case "START_GAME_REQUEST":
		case "UPDATE_QUESTION_REQUEST":
			return {
				...state,
				error: false,
				msgError: "",
				loading: true
			};
		case "START_GAME_SUCCESS":
		case "CONTINUE_GAME_SUCCESS":
		case "UPDATE_GAME_SUCCESS":
			return {
				...state,
				data: action.payload,
				loading: false
			};
		case "START_GAME_FAILURE":
		case "UPDATE_GAME_FAILURE":
			return {
				...state,
				data: {},
				error: true,
				msgError: action.payload,
				loading: false
			};

		// RESET GAME
		case "RESET_GAME":
			return INITIAL_VALUE;

		default:
			return state;
	}
};
