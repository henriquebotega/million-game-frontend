import api from "../../services/api";

// const shuffle = o => {
// 	for (var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
// 	return o;
// };

const getRandomNumber = (limit, nRandomDif) => {
	let n = Math.floor(Math.random() * Math.floor(limit));

	if (n === nRandomDif) {
		n = getRandomNumber(limit, nRandomDif);
	}

	return n;
};

export const getLevel = () => {
	return dispatch => {
		return new Promise((resolve, reject) => {
			dispatch({ type: "GET_LEVEL_REQUEST" });

			if (localStorage.getItem("levels")) {
				const currentLevels = JSON.parse(localStorage.getItem("levels"));
				dispatch({ type: "GET_LEVEL_SUCCESS", payload: currentLevels });
				resolve(currentLevels);
			} else {
				api.get("/level")
					.then(res => {
						localStorage.setItem("levels", JSON.stringify(res.data));
						dispatch({ type: "GET_LEVEL_SUCCESS", payload: res.data });
						resolve(res.data);
					})
					.catch(error => {
						dispatch({ type: "GET_LEVEL_FAILURE", payload: error });
						reject();
					});
			}
		});
	};
};

export const createUser = form => {
	return dispatch => {
		return new Promise((resolve, reject) => {
			dispatch({ type: "CREATE_USER_REQUEST" });

			if (localStorage.getItem("user")) {
				const currentUser = JSON.parse(localStorage.getItem("user"));
				dispatch({ type: "CREATE_USER_SUCCESS", payload: currentUser });
				resolve(currentUser);
			} else {
				api.post("/user", form)
					.then(res => {
						localStorage.setItem("user", JSON.stringify(res.data));
						dispatch({ type: "CREATE_USER_SUCCESS", payload: res.data });
						resolve(res.data);
					})
					.catch(error => {
						dispatch({ type: "CREATE_USER_FAILURE", payload: error });
						reject();
					});
			}
		});
	};
};

export const getQuestion = level => {
	return dispatch => {
		return new Promise((resolve, reject) => {
			dispatch({ type: "GET_QUESTION_REQUEST" });

			const game = JSON.parse(localStorage.getItem("game"));

			const extraHelps = {
				skippedQuestions: game && game._skippedQuestions.length > 0 ? game._skippedQuestions.map(i => i._id) : [],
				fiftyQuestion: game && game._fiftyQuestion ? game._fiftyQuestion._id : null
			};

			if (localStorage.getItem("question")) {
				const currentQuestion = JSON.parse(localStorage.getItem("question"));
				dispatch({ type: "GET_QUESTION_SUCCESS", payload: currentQuestion });
				resolve(currentQuestion);
			} else {
				api.post("/question/level/" + level, extraHelps)
					.then(res => {
						localStorage.setItem("question", JSON.stringify(res.data));
						dispatch({ type: "GET_QUESTION_SUCCESS", payload: res.data });
						resolve(res.data);

						// Alter the game
						alterGame(dispatch, game, { _question: res.data._id, _level: res.data._level._id });
					})
					.catch(error => {
						dispatch({ type: "GET_QUESTION_FAILURE", payload: error });
						reject();
					});
			}
		});
	};
};

export const getAnswerByQuestion = question => {
	return dispatch => {
		return new Promise((resolve, reject) => {
			dispatch({ type: "GET_ANSWER_BY_QUESTION_REQUEST" });

			if (localStorage.getItem("answer_by_question")) {
				const currentAnswerByQuestion = JSON.parse(localStorage.getItem("answer_by_question"));
				dispatch({ type: "GET_ANSWER_BY_QUESTION_SUCCESS", payload: currentAnswerByQuestion });
				resolve(currentAnswerByQuestion);
			} else {
				api.get("/answer/question/" + question)
					.then(res => {
						localStorage.setItem("answer_by_question", JSON.stringify(res.data));
						dispatch({ type: "GET_ANSWER_BY_QUESTION_SUCCESS", payload: res.data });
						resolve(res.data);
					})
					.catch(error => {
						dispatch({ type: "GET_ANSWER_BY_QUESTION_FAILURE", payload: error });
						reject();
					});
			}
		});
	};
};

export const skipQuestion = (game, question) => {
	return dispatch => {
		return new Promise((resolve, reject) => {
			dispatch({ type: "SKIP_QUESTION_REQUEST" });

			localStorage.removeItem("question");
			localStorage.removeItem("answer_by_question");
			dispatch(resetQuestion());
			dispatch(resetAnswer());

			api.put("/game/" + game + "/skip/" + question)
				.then(res => {
					localStorage.setItem("game", JSON.stringify(res.data));
					dispatch({ type: "SKIP_QUESTION_SUCCESS", payload: res.data });

					// Continue the game
					continueGame(dispatch);

					resolve(res.data);
				})
				.catch(error => {
					dispatch({ type: "SKIP_QUESTION_FAILURE", payload: error });
					reject();
				});
		});
	};
};

export const finishingGame = (game, reward) => {
	return dispatch => {
		api.put("/game/" + game + "/finishing", { reward });
	};
};

export const fiftyQuestion = (game, question, answers) => {
	return dispatch => {
		return new Promise((resolve, reject) => {
			dispatch({ type: "FIFTY_QUESTION_REQUEST" });

			api.put("/game/" + game + "/fifty/" + question)
				.then(res => {
					localStorage.setItem("game", JSON.stringify(res.data));
					dispatch({ type: "FIFTY_QUESTION_SUCCESS", payload: res.data });

					const nRandomDif = answers.findIndex(i => i.isValid);
					const nRandom = getRandomNumber(answers.length, nRandomDif);

					let answer_fifty = answers.map((obj, i) => {
						obj.isDisabled = false;

						if (obj.isValid === false && i !== nRandom) {
							obj.isDisabled = true;
						}

						return obj;
					});

					localStorage.setItem("answer_by_question", JSON.stringify(answer_fifty));
					dispatch({ type: "GET_ANSWER_BY_QUESTION_SUCCESS", payload: answer_fifty });

					resolve(res.data);
				})
				.catch(error => {
					dispatch({ type: "FIFTY_QUESTION_FAILURE", payload: error });
					reject();
				});
		});
	};
};

const alterGame = (dispatch, game, data) => {
	dispatch({ type: "UPDATE_QUESTION_REQUEST" });

	if (game) {
		api.put("/game/" + game._id, data)
			.then(res => {
				localStorage.setItem("game", JSON.stringify(res.data));
				dispatch({ type: "UPDATE_GAME_SUCCESS", payload: res.data });
			})
			.catch(error => {
				dispatch({ type: "UPDATE_GAME_FAILURE", payload: error });
			});
	}
};

const continueGame = dispatch => {
	// Get your current game
	const currentGame = JSON.parse(localStorage.getItem("game"));

	// Get one question from your current level
	dispatch(getQuestion(currentGame._level._id)).then(currentQuestion => {
		// Get the answers of this question
		dispatch(getAnswerByQuestion(currentQuestion._id)).then(() => {
			dispatch({ type: "CONTINUE_GAME_SUCCESS", payload: currentGame });
		});
	});
};

export const resetGame = history => {
	return dispatch => {
		dispatch({ type: "RESET_GAME" });
		history.replace("/login");
	};
};

export const resetAnswer = () => {
	return dispatch => {
		dispatch({ type: "RESET_ANSWER" });
	};
};

export const resetQuestion = () => {
	return dispatch => {
		dispatch({ type: "RESET_QUESTION" });
	};
};

export const startGame = () => {
	return dispatch => {
		return new Promise((resolve, reject) => {
			// Find all Levels
			dispatch(getLevel()).then(currentLevel => {
				// Get current user online
				const currentUser = JSON.parse(localStorage.getItem("user"));

				if (localStorage.getItem("game")) {
					// Continue the game
					continueGame(dispatch);
					resolve(true);
				} else {
					// Get your first question
					dispatch(getQuestion(currentLevel[0]._id)).then(currentQuestion => {
						// Get the answers of this question
						dispatch(getAnswerByQuestion(currentQuestion._id)).then(() => {
							// Create and start a new game
							dispatch({ type: "START_GAME_REQUEST" });

							const myNewGame = {
								_level: currentLevel[0]._id,
								_user: currentUser._id,
								_question: currentQuestion._id,
								skip: 3,
								fifty: false
							};

							api.post("/game", myNewGame)
								.then(res => {
									localStorage.setItem("game", JSON.stringify(res.data));
									dispatch({ type: "START_GAME_SUCCESS", payload: res.data });
									resolve(res.data);
								})
								.catch(error => {
									dispatch({ type: "START_GAME_FAILURE", payload: error });
									reject();
								});
						});
					});
				}
			});
		});
	};
};
