import cogoToast from "cogo-toast";
import React, { useState } from "react";
import { Button, Form } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { createUser } from "../../store/actions";
import "./styles.css";

const Login = () => {
	const [form, setForm] = useState({
		name: "",
		email: ""
	});

	const history = useHistory();
	const dispatch = useDispatch();
	const userReducer = useSelector(state => state.userReducer);

	const changeText = e => {
		setForm({
			...form,
			[e.target.name]: e.target.value
		});
	};

	const listRecords = () => {
		history.replace("/records");
	};

	const validar = async e => {
		e.preventDefault();

		if (form.name && form.email) {
			dispatch(createUser(form))
				.then(() => {
					history.replace("/");
				})
				.catch(() => {
					cogoToast.warn("This e-mail is already in use!", { position: "bottom-right" });
				});
		}
	};

	return (
		<div className="fundo">
			<Form onSubmit={e => validar(e)}>
				<Form.Group controlId="formName">
					<Form.Label>Name</Form.Label>
					<Form.Control type="text" placeholder="Enter your name" value={form.name} name="name" onChange={e => changeText(e)} />
				</Form.Group>

				<Form.Group controlId="formEmail">
					<Form.Label>E-mail</Form.Label>
					<Form.Control type="email" placeholder="Enter your email" value={form.email} name="email" onChange={e => changeText(e)} />
				</Form.Group>

				<Button type="button" variant="info" onClick={() => listRecords()}>
					List Records
				</Button>

				<Button type="submit" className="validate" variant="primary" disabled={userReducer.loading}>
					Validate
				</Button>
			</Form>
		</div>
	);
};

export default Login;
