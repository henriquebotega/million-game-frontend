import cogoToast from "cogo-toast";
import React, { useEffect, useState } from "react";
import { Badge, Button } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { useSpeech } from "react-web-voice";
import Swal from "sweetalert2";
import { fiftyQuestion, finishingGame, getAnswerByQuestion, getQuestion, resetAnswer, resetGame, resetQuestion, skipQuestion, startGame } from "../../store/actions";
import "./styles.css";

const Home = () => {
	const { speak } = useSpeech({ voice: "Karen" });

	const [currentItem, setCurrentItem] = useState(null);
	const [posI, setPosI] = useState(0);

	const history = useHistory();
	const dispatch = useDispatch();
	const gameReducer = useSelector(state => state.gameReducer);
	const questionReducer = useSelector(state => state.questionReducer);
	const answerReducer = useSelector(state => state.answerReducer);
	const levelReducer = useSelector(state => state.levelReducer);

	const { loading: lLoading, data: lData } = levelReducer;
	const { loading: gLoading, data: gData } = gameReducer;
	const { loading: qLoading, data: qData } = questionReducer;
	const { loading: aLoading, data: aData } = answerReducer;

	useEffect(() => {
		dispatch(startGame());
		welcomeUser();
	}, []);

	const welcomeUser = async () => {
		if (localStorage.getItem("user")) {
			await speak({ text: `Welcome ${JSON.parse(localStorage.getItem("user")).name}, Let's start the game` });
		}
	};

	useEffect(() => {
		readQuestion();
	}, [questionReducer]);

	const readQuestion = async () => {
		if (!qLoading && qData.description) {
			await speak({ text: qData.description });
		}
	};

	const youAreWrong = async e => {
		Swal.fire("Sorry", "You are wrong, you lost everything that you had.", "error");
		await speak({ text: "You are wrong. You lost everything that you had." });

		dispatch(finishingGame(gData._id, "None"));
		logout();
	};

	const youWin = async e => {
		Swal.fire("Congratulations", "You Win!", "success");
		await speak({ text: "Congratulations. You win 1 million dollars" });

		dispatch(finishingGame(gData._id, "1 million"));
		logout();
	};

	const giveup = async e => {
		Swal.fire("You give up", `You will receive ${gData._level?.rewardGiveup} dollars`, "info");
		await speak({ text: `You will receive $${gData._level?.rewardGiveup} dollars` });

		dispatch(finishingGame(gData._id, gData._level.rewardGiveup + " thousand"));
		logout();
	};

	const logout = e => {
		localStorage.clear();
		dispatch(resetGame(history));
	};

	const skip = e => {
		setPosI(null);
		const gameId = gData._id;
		const questionId = gData._question._id;

		dispatch(skipQuestion(gameId, questionId));
	};

	const fifty = e => {
		setPosI(null);
		const gameId = gData._id;
		const questionId = gData._question._id;

		dispatch(fiftyQuestion(gameId, questionId, aData));
	};

	const doubleCheck = async (item, i) => {
		setPosI(i);
		setCurrentItem(item);

		Swal.fire({
			title: "Are you sure?",
			icon: "info",
			showCancelButton: true,
			confirmButtonText: "Yes!",
			cancelButtonText: "No"
		})
			.then(result => {
				if (result.value) {
					checkItem(item);
				} else {
					setCurrentItem(null);
				}
			})
			.catch(() => {
				setCurrentItem(null);
			});

		await speak({ text: `You choose ${item.description}, Are you sure?` });
	};

	const checkItem = async item => {
		if (item.isValid) {
			let nextPosition = null;

			const currentLevel = gData._level;
			const currentPosition = lData.findIndex(s => s._id === currentLevel._id);

			// If not the last position
			if (currentPosition !== lData.length - 1) {
				nextPosition = lData[currentPosition + 1];
			}

			if (nextPosition) {
				cogoToast.success(`You win ${currentLevel.rewardWin}, next question!`, { position: "bottom-right" });
				await speak({ text: `You win ${currentLevel.rewardWin} dollars, next question` });

				setPosI(null);
				localStorage.removeItem("question");
				localStorage.removeItem("answer_by_question");

				dispatch(resetQuestion());
				dispatch(resetAnswer());

				const gameId = gData._id;
				dispatch(finishingGame(gameId, currentLevel.description));

				// Get the next question
				dispatch(getQuestion(nextPosition._id)).then(currentQuestion => {
					// Get the answers of this question
					dispatch(getAnswerByQuestion(currentQuestion._id));
				});
			} else {
				youWin();
			}
		} else {
			youAreWrong();
		}
	};

	return (
		<div className="home">
			{lLoading || gLoading || qLoading || aLoading ? (
				<div>Loading...</div>
			) : (
				<div>
					{!qLoading && qData.description && <h2>{qData.description}</h2>}

					{!aLoading && aData.length > 0 && (
						<ul>
							{aData.map((item, i) => {
								return (
									<li key={i}>
										<Button className="btnAnswer" disabled={item.isDisabled} style={{ backgroundColor: currentItem && i === posI ? "green" : "" }} type="button" variant="secondary" onClick={() => doubleCheck(item, i)}>
											<div>{i + 1}</div>
											{item.description}
										</Button>
									</li>
								);
							})}
						</ul>
					)}

					<Button className="btnGiveup" variant="danger" type="button" onClick={() => giveup()}>
						Give up
					</Button>

					<Button className="btnSkip" variant="warning" type="button" onClick={() => skip()} disabled={gData.skip === 0}>
						Skip <Badge variant="light">{gData.skip}</Badge>
					</Button>

					<Button className="btnFifty" variant="warning" type="button" onClick={() => fifty()} disabled={gData.fifty}>
						Fifty
					</Button>

					<div className="noticeReward">
						<p>
							<b>You already won ${gData._level?.rewardGiveup} dollars</b>
						</p>
						If you get it right, you will win ${gData._level?.rewardWin} dollars
						<br />
						If you giveup, you will receive ${gData._level?.rewardGiveup} dollars
					</div>
				</div>
			)}
		</div>
	);
};

export default Home;
