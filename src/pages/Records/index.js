import React, { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import api from "../../services/api";
import "./styles.css";

const Records = () => {
	const [topten, setTopten] = useState([]);

	const history = useHistory();

	useEffect(() => {
		api.get("/topten").then(res => {
			setTopten(res.data);
		});
	}, []);

	const voltar = () => {
		history.replace("/");
	};

	const formatDate = dt => {
		const d = dt.substr(8, 2);
		const m = dt.substr(5, 2);
		const a = dt.substr(0, 4);

		return a + "/" + m + "/" + d;
	};

	return (
		<div className="content">
			<div className="box">
				<h1>Top Ten</h1>

				{topten.map((obj, i) => {
					return (
						<div key={i} className={`boxGame boxGameBlack ${obj.reward === "1 million" && "boxGameGreen"} ${obj.reward === "None" && "boxGameRed"}`}>
							<div className="name">{obj._user.name}</div>
							<div className="value">{obj.reward}</div>
							<div className="date">{formatDate(obj.updatedAt)}</div>
						</div>
					);
				})}

				<div className="fimTable"></div>

				<Button type="button" variant="info" onClick={() => voltar()}>
					Back to Login
				</Button>
			</div>
		</div>
	);
};

export default Records;
