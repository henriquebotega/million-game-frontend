import React from "react";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Redirect, Route, Switch } from "react-router-dom";
import "sweetalert2/src/sweetalert2.scss";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Records from "./pages/Records";
import store from "./store";

const PrivateRoute = ({ children, ...rest }) => {
	const isAuthenticated = localStorage.getItem("user");
	return <Route {...rest} render={() => (isAuthenticated ? children : <Redirect to={{ pathname: "/login" }} />)} />;
};

const App = () => {
	return (
		<Provider store={store}>
			<Router>
				<Switch>
					<Route path="/login">
						<Login />
					</Route>
					<Route path="/records">
						<Records />
					</Route>
					<PrivateRoute path="/">
						<Home />
					</PrivateRoute>
				</Switch>
			</Router>
		</Provider>
	);
};

export default App;
